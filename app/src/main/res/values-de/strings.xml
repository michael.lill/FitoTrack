<?xml version="1.0" encoding="utf-8"?><!--
  ~ Copyright (c) 2020 Jannis Scheibe <jannis@tadris.de>
  ~
  ~ This file is part of FitoTrack
  ~
  ~ FitoTrack is free software: you can redistribute it and/or modify
  ~     it under the terms of the GNU General Public License as published by
  ~     the Free Software Foundation, either version 3 of the License, or
  ~     (at your option) any later version.
  ~
  ~     FitoTrack is distributed in the hope that it will be useful,
  ~     but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~     GNU General Public License for more details.
  ~
  ~     You should have received a copy of the GNU General Public License
  ~     along with this program.  If not, see <http://www.gnu.org/licenses/>.
  -->

<resources>
    <string name="app_name">FitoTrack</string>
    <string name="backup">Backup</string>
    <string name="cancel">Abbrechen</string>
    <string name="chooseBackupFile">Wähle die Backup-Datei</string>
    <string name="comment">Kommentar</string>
    <string name="continue_">Weiter</string>
    <string name="converting">Konvertieren</string>
    <string name="delete">Löschen</string>
    <string name="deleteWorkout">Workout löschen</string>
    <string name="deleteWorkoutMessage">Möchtest du das Workout wirklich löschen?</string>
    <string name="enterComment">Kommentar eingeben</string>
    <string name="error">Fehler</string>
    <string name="errorExportFailed">Der Datenexport ist fehlgeschlagen.</string>
    <string name="errorGpxExportFailed">Der GPX-Export ist fehlgeschlagen.</string>
    <string name="errorImportFailed">Der Datenimport ist fehlgeschlagen.</string>
    <string name="exportAsGpxFile">Als GPX-Datei exportieren</string>
    <string name="exportData">Daten exportieren</string>
    <string name="exportDataSummary">Erstellt ein Backup aller Workouts</string>
    <string name="exporting">Exportieren</string>
    <string name="finished">Fertig</string>
    <string name="gps">GPS</string>
    <string name="importBackup">Daten-Backup importieren</string>
    <string name="importBackupMessage">WARNUNG: All deine aktuellen Daten in der App werden gelöscht. Wenn du diese nicht verlieren möchtest, mache vorher ein anderes Backup. Bist du dir sicher, dass du das Backup wiederherstellen möchtest?</string>
    <string name="importBackupSummary">Backup wiederherstellen</string>
    <string name="initialising">Initialisieren</string>
    <string name="loadingFile">Datei laden</string>
    <string name="locationData">Positionsdaten</string>
    <string name="okay">Okay</string>
    <string name="pref_ringtone_silent">Still</string>
    <string name="pref_unit_system">Bevorzugtes Einheitensystem</string>
    <string name="pref_weight">Dein Gewicht</string>
    <string name="pref_weight_summary">Dein Gewicht ist zur Kalorienberechnung wichtig</string>

    <string name="preferences">Einstellungen</string>
    <string name="recordWorkout">Workout aufzeichnen</string>
    <string name="restore">Wiederherstellen</string>
    <string name="setPreferencesMessage">Setze in den Einstellungen dein bevorzugtes Einheitensystem und Gewicht.</string>
    <string name="setPreferencesTitle">Einstellungen setzen</string>
    <string name="settings">Einstellungen</string>
    <string name="shareFile">Datei teilen</string>
    <string name="stop">Stopp</string>
    <string name="stopRecordingQuestion">Aufzeichnung stoppen?</string>
    <string name="stopRecordingQuestionMessage">Möchtest du die Aufzeichnung wirklich stoppen?</string>
    <string name="trackerRunning">Tracker läuft</string>
    <string name="trackerRunningMessage">Dein Workout wird aufgezeichnet</string>
    <string name="trackingInfo">Tracking Info</string>
    <string name="trackingInfoDescription">Info über den laufenden Tracker</string>
    <string name="workoutAvgSpeedShort">Durchschnittsgeschw.</string>
    <string name="workoutBurnedEnergy">Verbrannte Kalorien</string>
    <string name="workoutDate">Datum</string>
    <string name="workoutDistance">Distanz</string>
    <string name="workoutDuration">Dauer</string>
    <string name="workoutEndTime">Endzeit</string>
    <string name="workoutEnergyConsumption">Energieverbrauch</string>
    <string name="workoutPace">Pace</string>
    <string name="workoutPauseDuration">Pausenzeit</string>
    <string name="workoutRoute">Route</string>
    <string name="workoutSpeed">Geschwindigkeit</string>
    <string name="workoutStartTime">Startzeit</string>
    <string name="workoutTime">Zeit</string>
    <string name="workoutTopSpeed">Max Geschw.</string>
    <string name="workoutTotalEnergy">Gesamte Energie</string>
    <string name="workoutTypeCycling">Fahrrad fahren</string>
    <string name="workoutTypeHiking">Wandern</string>
    <string name="workoutTypeJogging">Laufen</string>
    <string name="workoutTypeRunning">Rennen</string>
    <string name="workoutTypeUnknown">Unbekannt</string>
    <string name="workoutTypeWalking">Gehen</string>
    <string name="workout_add">Hinzufügen</string>
    <string name="workouts">Workouts</string>
    <string name="enable">Aktivieren</string>
    <string name="height">Höhe</string>
    <string name="noGpsTitle">GPS deaktiviert</string>
    <string name="noGpsMessage">Bitte aktiviere GPS, damit dein Workout aufgezeichnet werden kann.</string>
    <string name="workoutAscent">Aufstieg</string>
    <string name="workoutDescent">Abstieg</string>
    <string name="data">Daten</string>
    <string name="mapStyle">Kartenstil</string>
    <string name="actionUploadToOSM">Auf OSM hochladen</string>
    <string name="authenticationFailed">Authentifizierung fehlgeschlagen.</string>
    <string name="cut">Die ersten/letzten 300 m abschneiden</string>
    <string name="description">Beschreibung</string>
    <string name="enterVerificationCode">Gebe den Verifizierungscode ein</string>
    <string name="trackVisibilityPref">Spur Sichtbarkeit</string>
    <string name="upload">Hochladen</string>
    <string name="uploading">Hochladen</string>
    <string name="waiting_gps">Warte auf GPS</string>
    <string name="uploadFailed">Upload fehlgeschlagen</string>
    <string name="uploadSuccessful">Upload erfolgreich</string>
    <string name="uploadFailedOsmNotAuthorized">Nicht autorisiert, noch einmal versuchen</string>
    <string name="ttsNotAvailable">Sprachausgabe ist nicht verfügbar</string>
    <string name="workoutAvgSpeedLong">Durchschnittsgeschwindigkeit</string>
    <string name="and">und</string>
    <string name="announcementGPSStatus">GPS Status</string>
    <string name="gpsFound">GPS Signal gefunden</string>
    <string name="gpsLost">GPS Signal verloren</string>
    <string name="pref_announcements_config_summary">Wähle die Zeit und Distanz zwischen den Sprachansagen</string>
    <string name="pref_announcements_config_title">Auslöser der Ansage</string>
    <string name="pref_announcements_content">Inhalt der Ansagen</string>
    <string name="pref_voice_announcements_summary">Konfiguriere Sprachansagen, die dich während des Workouts informieren</string>
    <string name="timeHourPlural">Stunden</string>
    <string name="timeHourSingular">Stunde</string>
    <string name="timeMinutePlural">Minuten</string>
    <string name="timeMinuteSingular">Minute</string>
    <string name="voiceAnnouncementsTitle">Sprachansagen</string>
    <string name="action_edit_comment">Kommentar bearbeiten</string>
    <string name="pref_announcement_mode">Modus der Ansagen</string>
    <string name="save">Speichern</string>
    <string name="savedToDownloads">Im Downloadordner gespeichert</string>
    <string name="savingFailed">Speichern fehlgeschlagen</string>
    <string name="share">Teilen</string>
    <string name="timeMinuteShort">min</string>
    <string name="timeHourShort">h</string>
    <string name="timeSecondsShort">s</string>
    <string name="errorEnterValidNumber">Gebe eine gültige Zahl ein</string>
    <string name="errorWorkoutAddFuture">Das Workout kann sich nicht in der Zukunft befinden</string>
    <string name="errorEnterValidDuration">Gebe eine gültige Dauer ein</string>
    <string name="workoutEdited">Dieses Workout wurde bearbeitet.</string>
    <string name="workoutTypeOther">Anderes</string>
    <string name="type">Typ</string>
    <string name="enterWorkout">Workout eingeben</string>
    <string name="importWorkout">Workout importieren</string>
    <string name="setDuration">Dauer setzen</string>
    <string name="info">Info</string>
    <string name="OpenStreetMapAttribution">© OpenStreetMap Mitwirkende</string>
    <string name="theme">Thema</string>
    <string name="hintRestart">Bitte die App neustarten, damit die Änderungen wirksam werden</string>
    <string name="noComment">Kein Kommentar</string>
    <string name="sections">Abschnitte</string>

    <string-array name="pref_announcement_mode">
        <item>Immer</item>
        <item>Nur mit Kopfhörern</item>
    </string-array>

    <string-array name="pref_map_layers">
        <item>Mapnik\nStandard OSM Kartenstil</item>
        <item>Humanitarian\nSchöne Karte, aber manchmal nicht verfügbar</item>
        <item>Outdoors\nLimitierter Zugriff</item>
        <item>Cyclemap\nLimitierter Zugriff</item>
    </string-array>

    <string-array name="osm_track_visibility">
        <item>Identifizierbar</item>
        <item>Verfolgbar</item>
        <item>Privat</item>
    </string-array>

    <string-array name="pref_theme_setting">
        <item>Hell</item>
        <item>Dunkel</item>
    </string-array>

    <string-array name="pref_unit_systems">
        <item>Metrisch</item>
        <item>Physikalische Größen (m/s)</item>
        <item>Imperial</item>
        <item>Imperial mit Metern</item>
    </string-array>
    <string name="start">Start</string>
    <string name="cannotStart">Starten unmöglich</string>
    <string name="add">Hinzufügen</string>
    <string name="add_interval">Interval Hinzufügen</string>
    <string name="manageIntervalSets">Intervallsätze bearbeiten</string>
    <string name="manageIntervalsSummary">Bearbeite die Intervalsätze, die du dann während des Workouts über Sprachansagen nutzen kannst.</string>
    <string name="workoutRecording">Workout-Aufzeichnung</string>
    <string name="createIntervalSet">Neuen Intervallsatz erstellen</string>
    <string name="create">Erstellen</string>
    <string name="hintManageIntervalSets">Füge ein Intervall-Set hinzu, um es in deinen Workouts zu verwenden.</string>
    <string name="hintEditIntervalSet">Füge Intervalle hinzu, die gesprochen werden sollen.</string>
    <string name="interval_name">Name:</string>
    <string name="length_in_minutes">Länge (Minuten):</string>
    <string name="enterName">Gebe einen Namen ein</string>
    <string name="deleteIntervalSet">Intervallsatz löschen</string>
    <string name="deleteIntervalSetMessage">Möchtest du den Intervallsatz löschen?</string>
    <string name="selectIntervalSet">Intervallsatz auswählen</string>
    <string name="intervalSetSelected">Intervallsatz ausgewählt</string>
    <string name="intervalsIncludePauseTitle">Pausenzeit in das Intervalltraining einbeziehen</string>
    <string name="intervalsIncludePauseSummary">Wenn aktiv, werden die Intervalle auch dann fortgesetzt, wenn du Pausen einlegst.</string>
    <string name="intervalTraining">Intervalltraining</string>
    <string name="intervalSet">Intervallsatz</string>
    <string name="intervalSets">Intervallsätze</string>
    <string name="currentSpeed">Aktuelle Geschwindigkeit</string>
    <string name="hintAddWorkout">Klicke auf das Plus-Symbol, um ein Workout aufzuzeichnen oder einzugeben.</string>
    <string name="enterDescription">Gebe eine Beschreibung ein</string>
    <string name="avgSpeedInMotion">Durchschnittsgeschw. (in Bewegung)</string>
    <string name="avgSpeedTotalShort">Durchschnittsgeschw. (Gesamt)</string>
    <string name="avgSpeedTotalLong">Durchschnittsgeschwindigkeit im Gesamten</string>
    <string name="preferencesUserInterfaceTitle">Benutzeroberfläche</string>
    <string name="preferencesUserInterfaceSummary">Personalisiere die App</string>
    <string name="preferencesRecordingTitle">Aufzeichnung</string>
    <string name="preferencesRecordingSummary">Verwalten der Aufzeichnung von Workouts</string>
    <string name="preferencesBackupTitle">Datenbank</string>
    <string name="preferencesBackupSummary">Sichern/Wiederherstellen deiner Workouts</string>
    <string name="preferencesCategoryAppearance">Erscheinungsbild</string>
    <string name="preferenceDateFormat">Datumsformat</string>
    <string name="preferenceTimeFormat">Zeitformat</string>
    <string name="workoutDiscarded">Workout verworfen</string>
    <string name="timeSecondsSingular">Sekunde</string>
    <string name="timeSecondsPlural">Sekunden</string>
    <string name="per">pro</string>
    <string name="unitMilesSingular">Meile</string>
    <string name="unitMilesPlural">Meilen</string>
    <string name="unitYardsSingular">Yard</string>
    <string name="unitYardsPlural">Yards</string>
    <string name="unitMilesPerHour">Meilen pro Stunde</string>
    <string name="unitMetersPlural">Meter</string>
    <string name="unitMetersSingular">Meter</string>
    <string name="unitKilometersSingular">Kilometer</string>
    <string name="unitKilometersPlural">Kilometer</string>
    <string name="unitKilometersPerHour">Kilometer pro Stunde</string>
    <string name="unitMetersPerSecond">Meter pro Sekunde</string>
    <string name="unitKcalLong">Kalorien</string>
    <string name="unitKJoule">Kilojoule</string>
    <string name="pref_energy_unit">Energieeinheit</string>
    <string name="workoutTypeInlineSkating">Inline Skating</string>
    <string name="manageAutoTimeoutSummary">Einstellung zum automatisch Stoppen des Workouts</string>
    <string name="intervalAutoTimeoutTraining">Workout Auto-Stop Timer</string>
    <string name="pref_auto_timeout_title">Zeitspanne bis Auto-Stop</string>
    <string name="currentTime">Aktuelle Uhrzeit</string>
    <string name="oClock">Uhr</string>
    <string name="unlockPhoneStopWorkout">Bitte das Telefon entsperren, um die Aufnahme zu beenden</string>
    <string name="workout_statistics">Workout Statistiken</string>
    <string name="workoutStatsHelpText">Dieser Screen zeigt historische Statistiken zu den Workouts. Um es zu verwenden, müssen man zunächst die Trainingsart angeben, indem man darauf klickt. Dann stellt man die Information, über die man etwas wissen möchte, und die Zeitspanne ein.\n\nWenn man z. B. \"Woche\" als Zeitspanne auswählt, werden alle Workouts jeder Woche in einem Datenpunkt zusammengefasst und entweder der Durchschnitt oder die Summe jeder Woche berechnet.</string>
    <string name="replaceOrMergeMessage">Datenbank ersetzen oder mit Backup zusammenführen?</string>
    <string name="replace">Ersetzen</string>
    <string name="merge">Zusammenführen</string>
    <string name="announcementSuppressDuringCall">Während Anruf unterdrücken</string>

    <string-array name="pref_energy_units">
        <item>kcal</item>
        <item>Joule</item>
    </string-array>

    <string-array name="pref_time_format">
        <item>System-Vorgabe</item>
        <item>12:08 PM</item>
        <item>15:23</item>
    </string-array>

    <string-array name="pref_date_format">
        <item>System-Vorgabe</item>
        <item>2001-07-24</item>
        <item>2001.07.24</item>
        <item>2001/07/24</item>
        <item>24-07-2001</item>
        <item>24.07.2001</item>
        <item>24/07/2001</item>
        <item>07/24/2001</item>
    </string-array>
    <string name="no_workouts_recorded_for_this_activity">Keine Workouts dieser Aktivität</string>
    <string name="day">Tag</string>
    <string name="week">Woche</string>
    <string name="month">Monat</string>
    <string name="year">Jahr</string>
    <string name="min">Min</string>
    <string name="max">Max</string>
    <string name="avg">Durchschn.</string>
    <string name="sum">Summe</string>
    <string name="workoutNumber">Anzahl der Workouts</string>
    <string name="statsFilter">Filtern:</string>
    <string name="help">Hilfe</string>
    <string name="dayInMonth">Tag des Monats</string>
    <string name="singleWorkout">Einzelnes Workout</string>
    <string name="calendarWeekYear">Kalendarwoche / Jahr</string>
    <string name="workoutImported">Workout erfolgreich importiert</string>

</resources>
